/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.firstframe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author WIP
 */
public class Frame {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  //    frame.setSize(new Dimension(500, 300));
        frame.setSize(500, 300); //overloading
        
        JLabel lbHelloWorld = new JLabel("Hello World.", JLabel.CENTER);
        lbHelloWorld.setBackground(Color.CYAN);
        lbHelloWorld.setFont(new Font("Verdana", Font.PLAIN, 25));
        lbHelloWorld.setOpaque(true);
        frame.add(lbHelloWorld);
        
        
        
        frame.setVisible(true);
        
    }
}
