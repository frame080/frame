/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.firstframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener: Action");
    }
    
}

/**
 *
 * @author WIP
 */
public class Helloto implements ActionListener {
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame("Hello");
        mainFrame.setSize(500, 300);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lbYourName = new JLabel("Your Name: ");
        lbYourName.setSize(70, 20);
        lbYourName.setLocation(5, 5);
        lbYourName.setBackground(Color.WHITE);
        lbYourName.setOpaque(true);
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(85, 5);
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(140, 35);
        
        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new Helloto());
        
        ActionListener actionListener = new ActionListener(){ //Anonymouse Class
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class: Action");
            }
            
        };
        btnHello.addActionListener(actionListener);
   

        JLabel lbHello = new JLabel("Hello...", JLabel.CENTER);
        lbHello.setSize(200, 20);
        lbHello.setLocation(85, 70);
        lbHello.setBackground(Color.WHITE);
        lbHello.setOpaque(true);
        
        mainFrame.setLayout(null);
        
        mainFrame.add(lbYourName);
        mainFrame.add(txtYourName);
        mainFrame.add(btnHello);
        mainFrame.add(lbHello);
        
        btnHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                 String name = txtYourName.getText();
                 lbHello.setText("Hello " + name);
            }
            
        });
        
        
        
        mainFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Helloto: Action");
    }
}
